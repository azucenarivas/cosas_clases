-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema central_transporte_azucena
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema central_transporte_azucena
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `central_transporte_azucena` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema facturacion
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema facturacion
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `facturacion` DEFAULT CHARACTER SET utf8 ;
USE `central_transporte_azucena` ;

-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`motoristas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`motoristas` (
  `id_motoristas` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) NOT NULL,
  `apellido` VARCHAR(60) NOT NULL,
  `telefono` VARCHAR(60) NOT NULL,
  `dui` VARCHAR(20) NOT NULL,
  `licencia` VARCHAR(20) NOT NULL,
  `direccion` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_motoristas`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`personas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`personas` (
  `id_personas` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) NOT NULL,
  `apellido` VARCHAR(60) NOT NULL,
  `telefono` VARCHAR(60) NOT NULL,
  `dui` VARCHAR(20) NOT NULL,
  `direccion` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_personas`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`unidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`unidades` (
  `id_unidades` INT(11) NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(8) NOT NULL,
  `capacidad` INT(11) NOT NULL,
  `ruta` VARCHAR(100) NOT NULL,
  `id_personas` INT(11) NOT NULL,
  `estado` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_unidades`),
  INDEX `fk_personas` (`id_personas` ASC) VISIBLE,
  CONSTRAINT `fk_personas`
    FOREIGN KEY (`id_personas`)
    REFERENCES `central_transporte_azucena`.`personas` (`id_personas`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`programacion_salidas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`programacion_salidas` (
  `id_programaciones` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `id_unidades` INT(11) NOT NULL,
  `id_motoristas` INT(11) NOT NULL,
  `punto_salida` VARCHAR(100) NOT NULL,
  `hora_salida` VARCHAR(60) NOT NULL,
  `punto_llegada` VARCHAR(100) NOT NULL,
  `hora_llegada` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_programaciones`),
  INDEX `fk_unidades` (`id_unidades` ASC) VISIBLE,
  INDEX `fk_motoristas1` (`id_motoristas` ASC) VISIBLE,
  CONSTRAINT `fk_motoristas1`
    FOREIGN KEY (`id_motoristas`)
    REFERENCES `central_transporte_azucena`.`motoristas` (`id_motoristas`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_unidades`
    FOREIGN KEY (`id_unidades`)
    REFERENCES `central_transporte_azucena`.`unidades` (`id_unidades`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`despachador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`despachador` (
  `id_despachador` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) NOT NULL,
  `apellido` VARCHAR(60) NOT NULL,
  `telefono` VARCHAR(60) NOT NULL,
  `id_programaciones` INT(11) NOT NULL,
  PRIMARY KEY (`id_despachador`),
  INDEX `fk_programacion_despachador` (`id_programaciones` ASC) VISIBLE,
  CONSTRAINT `fk_programacion_despachador`
    FOREIGN KEY (`id_programaciones`)
    REFERENCES `central_transporte_azucena`.`programacion_salidas` (`id_programaciones`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`roles` (
  `id_roles` INT(11) NOT NULL AUTO_INCREMENT,
  `cargo` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_roles`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `central_transporte_azucena`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `central_transporte_azucena`.`usuarios` (
  `id_personas` INT(11) NOT NULL,
  `usuario` VARCHAR(15) NOT NULL,
  `pass` VARCHAR(10) NOT NULL,
  `id_roles` INT(11) NOT NULL,
  PRIMARY KEY (`id_personas`),
  INDEX `fk_roles_usuarios` (`id_roles` ASC) VISIBLE,
  CONSTRAINT `fk_personas_usuarios`
    FOREIGN KEY (`id_personas`)
    REFERENCES `central_transporte_azucena`.`personas` (`id_personas`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_roles_usuarios`
    FOREIGN KEY (`id_roles`)
    REFERENCES `central_transporte_azucena`.`roles` (`id_roles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `facturacion` ;

-- -----------------------------------------------------
-- Table `facturacion`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`categoria` (
  `id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(15) NOT NULL,
  `descripcion` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_categoria`))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facturacion`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`cliente` (
  `id_cliente` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `apellido` VARCHAR(20) NOT NULL,
  `direccion` VARCHAR(40) NOT NULL,
  `fecha_nacimiento` DATE NULL DEFAULT NULL,
  `telefono` VARCHAR(9) NULL DEFAULT NULL,
  `email` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facturacion`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`producto` (
  `id_producto` INT(11) NOT NULL,
  `nombre` VARCHAR(15) NOT NULL,
  `precio` DOUBLE NOT NULL,
  `stock` INT(11) NOT NULL,
  `id_categoria` INT(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  INDEX `fk_categoria` (`id_categoria` ASC) VISIBLE,
  CONSTRAINT `FK_idcatep`
    FOREIGN KEY (`id_categoria`)
    REFERENCES `facturacion`.`categoria` (`id_categoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facturacion`.`modo_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`modo_pago` (
  `num_pago` INT(11) NOT NULL,
  `nombre` VARCHAR(20) NOT NULL,
  `otros_detalles` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`num_pago`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facturacion`.`factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`factura` (
  `num_factura` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(7) NOT NULL,
  `id_cliente` INT(11) NOT NULL,
  `fecha` DATE NOT NULL,
  `num_pago` INT(11) NOT NULL,
  PRIMARY KEY (`num_factura`),
  INDEX `fk_cliente` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_mod_pago` (`num_pago` ASC) VISIBLE,
  CONSTRAINT `FK_idclientef`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `facturacion`.`cliente` (`id_cliente`),
  CONSTRAINT `FK_pagomodo`
    FOREIGN KEY (`num_pago`)
    REFERENCES `facturacion`.`modo_pago` (`num_pago`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `facturacion`.`detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `facturacion`.`detalle` (
  `num_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `num_factura` INT(11) NOT NULL,
  `id_producto` INT(11) NOT NULL,
  `cantidad` INT(11) NOT NULL,
  `precio` DOUBLE NOT NULL,
  PRIMARY KEY (`num_detalle`, `num_factura`),
  INDEX `fk_factura` (`num_factura` ASC) VISIBLE,
  INDEX `fk_producto` (`id_producto` ASC) VISIBLE,
  CONSTRAINT `FK_idprod`
    FOREIGN KEY (`id_producto`)
    REFERENCES `facturacion`.`producto` (`id_producto`),
  CONSTRAINT `FK_numfact`
    FOREIGN KEY (`num_factura`)
    REFERENCES `facturacion`.`factura` (`num_factura`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
