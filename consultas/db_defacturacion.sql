use facturacion;

select *from categoria;

select *from cliente;

select *from detalle;

select *from factura;

select *from modo_pago;

select *from producto;

insert into categoria(nombre,descripcion)
values
('bebidas','liquidos'),
('comidas','de todo tipo'),
('verduras','frescas'),
('dulces','tipicos'),
('frutas','naturales'),
('electronicos','de todo tipo'),
('maderas','de todo tipo'),
('plantas','naturales'),
('quimicos','medicinas'),
('detergentes','liuidos,y secos');

insert into modo_pago(num_pago,nombre,otros_detalles)
values
(1,'efectivo','billetes y monedas'),
(2,'tarjeta de credito','billetes y monedas'),
(3,'tarjeta de regalo','tarjeta')

;


insert into cliente(id_cliente,nombre,apellido,direccion,fecha_nacimiento,telefono,email)
value
(4,'ada iveth','rivas corvera','soyapango','2000-02-02','7896-6325','adarivas@hotmail.com'),
(5,'suleyma guadalupe','rivas corvera','soyapango','2001-02-02','7896-6325','suley@hotmail.com'),
(6,'jose romeo','rivas corvera','soyapango','1999-02-02','7896-6325','romeo@hotmail.com'),
(7,'maria concepcion','corvera','san salvador','1990-02-02','7896-6325','maria@hotmail.com'),
(8,'jose alfredo','rivas','san salvador','1990-02-02','7896-6325','jose@hotmail.com'),
(9,'kelvin adonay','flores','san salvador','1992-02-02','7896-6325','kelvin@hotmail.com'),
(10,'elmer daniel','rivas','san salvador','1991-02-02','7896-6325','daniel@hotmail.com'),
(11,'walter wilfredo','romero','san salvador','1990-02-02','7896-6325','walter@hotmail.com'),
(12,'elmer enrique','melendez','san salvador','1990-02-02','7896-6325','elmer@hotmail.com'),
(13,'william enrique','melendez','san salvador','1990-02-02','7896-6325','william@hotmail.com');

insert into producto(id_producto,nombre,precio,stock,id_categoria)
values
(1,'agua natural','0.50','3',14),
(2,'agua mineral','0.50','3',14),
(3,'cocacola','1.00','5',14),
(4,'agua de coco','0.50','3',14),
(5,'agua accida','0.50','3',14),
(6,'jabon','0.50','3',23),
(7,'tv','200.00','3',19),
(8,'celular','250.00','3',19),
(9,'computadora','600.00','3',19),
(10,'planca','100.00','3',19);


insert into factura(num_factura,codigo,id_cliente,fecha,num_pago)
values
(1,'f001',1,'2020-02-21',1),
(2,'f002',2,'2020-02-21',1),
(3,'f002',3,'2020-02-22',1),
(4,'f002',3,'2020-11-23',2),
(5,'f002',4,'2020-11-24',2),
(6,'f002',5,'2020-11-25',2),
(7,'f002',6,'2020-12-26',3),
(8,'f002',7,'2020-12-26',3),
(9,'f002',8,'2020-12-27',3),
(10,'f002',9,'2020-12-28',3)
;
