package com.consulta.imp;


import java.util.List;

import org.hibernate.SessionFactory;

import com.consulta.models.Factura;
import com.consulta.utils.AbsFacade;
import com.consulta.utils.DAO;



public class FacturaRep extends AbsFacade<Factura> implements DAO<Factura>{

	SessionFactory starsession;
	public FacturaRep(SessionFactory sessionFactory) {
		super(Factura.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}

	
}
