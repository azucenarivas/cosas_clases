package com.consulta.utils;

import java.util.List;

public interface DAO<I> {

	public void create(I i);
	public List<I> read();
	public I readById(Object id);
	public void update(I i);
	public void delete(I i);
}
