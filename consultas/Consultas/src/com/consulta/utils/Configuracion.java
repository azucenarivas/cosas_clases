package com.consulta.utils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.consulta.imp.FacturaRep;


@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan("com.central")
public class Configuracion {

	@Bean//es una clase representativa de una extraccion de algo
	InternalResourceViewResolver transporte() {
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;

	}

	@Bean(name = "datasource")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://192.168.101.91:3306/central_transporte_azucena?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;

	}

	@Autowired//es una inyeccion a la base de datos osea una conexion
	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean startSession() {
		LocalSessionFactoryBean fabrica = new LocalSessionFactoryBean();
		fabrica.setDataSource(dataSource());
		fabrica.setPackagesToScan("com.consulta.models");
		fabrica.setHibernateProperties(getHibernateProperties());
		return fabrica;
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
		return tx;
	}

	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		// hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}
	
	@Autowired
	@Bean (name = "facturaR")
	public FacturaRep facturacion(SessionFactory sessionFactory) {
		return new FacturaRep(sessionFactory);
	}
	
	
}
