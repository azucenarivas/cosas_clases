package com.consulta.utils;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Inicializacion extends AbstractAnnotationConfigDispatcherServletInitializer{
	@Override
	protected Class<?> [] getRootConfigClasses(){
		return new Class[] {Configuracion.class};
	}
	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] {Configuracion.class};
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		return new String[] {"/"};
	}
	
}
