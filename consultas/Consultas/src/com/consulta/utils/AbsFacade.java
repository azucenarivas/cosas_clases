package com.consulta.utils;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbsFacade<T> {
	private Class<T> entityClass;

	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Autowired
	public abstract SessionFactory getSessionFactory();

	protected Session session;

	@Transactional
	public void create(T entity) {
		getSessionFactory().getCurrentSession().save(entity);
	}

	@Transactional
	public void update(T entity) {
		getSessionFactory().getCurrentSession().update(entity);
	}

	@Transactional
	public void delete(T entity) {
		getSessionFactory().getCurrentSession().delete(entity);
	}

	@Transactional
	public T readById(Object id) {
		return getSessionFactory().getCurrentSession().find(entityClass, id);
	}

	@Transactional
	public List<T> read() {
		session = getSessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();
	}
}
