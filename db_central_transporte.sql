
drop schema central_transporte_azucena;

create database if not exists central_transporte_azucena;
use central_transporte_azucena;


create table if not exists personas(
id_personas int not null primary key auto_increment,
nombre varchar(60)not null,
apellido varchar(60)not null,
telefono varchar(60)not null,
dui varchar(20)not null,
direccion varchar(60)not null
)engine InnoDB;

create table if not exists roles(
id_roles int not null primary key auto_increment,
cargo varchar(60) not null
)engine InnoDB;


create table if not exists usuarios(
id_personas int not null primary key,
usuario varchar(15) not null,
pass varchar(10) not null,
id_roles int not null,
constraint fk_personas_usuarios foreign key (id_personas) references personas(id_personas) on delete cascade on update cascade,
constraint fk_roles_usuarios foreign key (id_roles) references roles(id_roles) on delete cascade on update cascade
)engine InnoDB;

create table if not exists motoristas(
id_motoristas int not null primary key auto_increment,
nombre varchar(60)not null,
apellido varchar(60)not null,
telefono varchar(60)not null,
dui varchar(20)not null,
licencia varchar(20)not null,
direccion varchar(60)not null
)engine InnoDB;

create table if not exists unidades(
id_unidades int not null primary key auto_increment,
placa varchar(40)not null,
capacidad int not null,
ruta varchar(100)not null,
id_personas int not null,
estado varchar(60)not null,
constraint fk_personas foreign key(id_personas) references personas(id_personas)on update cascade on delete cascade
)engine InnoDB;

create table if not exists programacion_salidas(
id_programaciones int not null primary key auto_increment,
fecha date not null,
id_unidades int not null,
id_motoristas int not null,
punto_salida varchar(100)not null,
hora_salida varchar(60)not null,
punto_llegada varchar(100)not null,
hora_llegada varchar(60)not null,
id_personas int not null,
constraint fk_unidades foreign key(id_unidades) references unidades(id_unidades)on update cascade on delete cascade,
constraint fk_motoristas1 foreign key(id_motoristas) references motoristas(id_motoristas)on update cascade on delete cascade,
constraint fk_personas1 foreign key(id_personas) references personas(id_personas)on update cascade on delete cascade
)engine InnoDB;


insert into personas(nombre,apellido,telefono,dui,direccion)values('juan jose','cruz lopez','2356-9632','12369854-9','san salvador');
insert into roles(cargo)values('administrador');
insert into usuarios(id_personas,usuario,pass,id_roles)values(1,'juan','123',1);
insert into motoristas(nombre,apellido,telefono,dui,licencia,direccion)values('Edgar Alexander','cruz','2369-9968','78963258-9','L0014778','San Vicente san vicente'),('Jose Romeo','Rivas','2369-9968','78963258-9','L0014778','San Salvador san salvador');
insert into unidades(placa,capacidad,ruta,id_personas,estado)values('PL-00157',60,'116',1,'Activo'),('PL-00160',90,'113',1,'Activo');
insert into programacion_salidas(fecha,id_unidades,id_motoristas,punto_salida,hora_salida,punto_llegada,hora_llegada,id_personas)values('2020-02-06',1,1,'San Vicente','7:00 am','San Salvador','8:30 am',1),('2020-02-07',2,1,'San Salvador','9:00 am','San Vicente','10:30 am',1);

select *from usuarios;
select *from unidades;
select *from motoristas;
select *from unidades;
select *from programacion_salidas;
